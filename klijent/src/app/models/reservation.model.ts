import { User } from './user.model';
import { Screening } from './screenings.model';

export interface Reservation {
  _id: string;
  user: User | string;
  screening: Screening | string;
  seats: Array<string>;
}

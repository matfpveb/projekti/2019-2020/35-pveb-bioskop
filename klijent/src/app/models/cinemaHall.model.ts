export interface CinemaHall {
  _id: string;
  name: string;
  seats: Array<Array<string>>;
}

import { Observable, throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

export abstract class HttpErrorHandler {
  constructor(private router: Router) {}

  protected handleError() {
    return (error: HttpErrorResponse): Observable<never> => {
      if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred
        console.error('An error occurred:', error.error.message);
      } else {
        // The backend returned an unsuccessful response code.
        this.router.navigate([
          '/error',
          { message: error.message, statusCode: error.status },
        ]);
      }
      return throwError('Something bad happened; please try again later.');
    };
  }
}

export interface Movie {
  _id: string;
  name: string;
  synopsis: string;
  genre: string;
  director: string;
  actors: string;
  duration: string;
  image: string;
  youtube: string;
  recommended: boolean;
  carouselImage: string;
}

import { Movie } from './movie.model';
import { CinemaHall } from './cinemaHall.model';

export interface Screening {
  _id: string;
  movie: Movie | string;
  date: Date;
  showtime: string;
  cinemaHall: CinemaHall | string;
  seats: Array<Array<{seatId: string, booked: boolean}>>;
}

import { Component, Input, OnInit, Output } from '@angular/core';
import { Screening } from './../models/screenings.model';

@Component({
  selector: 'app-cinema-hall',
  templateUrl: './cinema-hall.component.html',
  styleUrls: ['./cinema-hall.component.css']
})
export class CinemaHallComponent implements OnInit {
  @Input()
  public hallSeats: Screening['seats'];

  constructor() {
  }

  ngOnInit(): void {
  }

}

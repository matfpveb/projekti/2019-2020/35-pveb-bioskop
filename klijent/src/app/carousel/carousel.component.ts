import { Component, OnInit } from '@angular/core';
import { MatCarousel, MatCarouselComponent } from '@ngmodule/material-carousel';
import { Movie } from './../models/movie.model';
import { Observable } from 'rxjs';
import { MoviesService } from './../services/movies.service';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {
  public recommendedMovies: Observable<Movie[]>;

  constructor(private moviesService: MoviesService) {
    this.recommendedMovies = this.moviesService.getRecommendedMovies();
  }

  ngOnInit(): void {
  }

}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { UsersService } from '../services/users.service';
import { Router } from '@angular/router';
import { MoviesService } from '../services/movies.service';
import { ScreeningsService } from '../services/screenings.service';
import { CinemaHallsService } from '../services/cinema-halls.service';
import { Movie } from './../models/movie.model';
import { Screening } from '../models/screenings.model';
import { CinemaHall } from '../models/cinemaHall.model';
import { InfoService } from '../services/info.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit, OnDestroy {
  movies: Movie[];
  screenings: Screening[];
  cinemaHalls: CinemaHall[];

  exampleCreateMovie = '';
  exampleCreateScreening = '';
  exampleCreateCinemaHall = '';
  exampleCreateInfo = '';

  activeSubscriptions: Subscription[] = [];

  constructor(private infoService: InfoService,
              private cinemaHallsService: CinemaHallsService,
              private screeningService: ScreeningsService,
              private moviesService: MoviesService,
              private usersService: UsersService,
              private router: Router) {
   }

  ngOnInit(): void {
    if (!this.usersService.isAdmin()) {
      this.router.navigateByUrl('/');
    }
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => {
      sub.unsubscribe(); });
  }

  public createMovie(data) {
    const sub = this.moviesService.createMovie(JSON.parse(data.value)).subscribe();
    this.activeSubscriptions.push(sub);
  }

  public getMovies() {
    const sub = this.moviesService.getMovies().subscribe(res => {
      this.movies = res;
    });
    this.activeSubscriptions.push(sub);
  }

  public deleteMovie(id) {
    const sub = this.moviesService.deleteMovie(id).subscribe();
    this.activeSubscriptions.push(sub);
  }

  public createScreening(data) {
    const sub = this.screeningService.createScreening(JSON.parse(data.value)).subscribe();
    this.activeSubscriptions.push(sub);
  }

  public getScreenings() {
    const sub = this.screeningService.getScreenings().subscribe(res => {
      this.screenings = res;
    });
    this.activeSubscriptions.push(sub);
  }

  public deleteScreening(id) {
    const sub = this.screeningService.deleteScreening(id).subscribe();
    this.activeSubscriptions.push(sub);
  }

  public createCinemaHall(data) {
    const sub = this.cinemaHallsService.createCinemaHall(JSON.parse(data.value)).subscribe();
    this.activeSubscriptions.push(sub);
  }

  public getCinemaHalls() {
    const sub = this.cinemaHallsService.getCinemaHalls().subscribe(res => {
      this.cinemaHalls = res;
    });
    this.activeSubscriptions.push(sub);
  }

  public deleteCinemaHall(id) {
    const sub = this.cinemaHallsService.deleteCinemaHall(id).subscribe();
    this.activeSubscriptions.push(sub);
  }

  public updateInfo(data) {
    const sub = this.infoService.updateInfo(JSON.parse(data.value)).subscribe();
    this.activeSubscriptions.push(sub);
  }

  public exampleInputForCreateMovie(): void {
    this.exampleCreateMovie = JSON.stringify({
      "name": "The Gentlemen",
      "synopsis": "Od reditelja Gaja Ričija , dolazi nova sofisticirana akciona komedija s velikim filmskim zvezdama u glavnim ulogama. Američki iseljenik Miki Person (Matthew McConaughey) je u Londonu izgradio visoko profitabilno carstvo marihuane. Kad se pročuje vest da se zauvek želi povući iz tog posla, pokreće se niz zavera, i podmićivanja i ucenjivanja u pokušaju da se zauzme njegova pozicija.",
      "genre": "akcija, komedija, kriminalistički",
      "director": "Guy Ritchie",
      "actors": "Matthew McConaughey, Charlie Hunnam, Michelle Dockery",
      "duration": "1h 53min",
      "image": "http://localhost:3000/uploads/theGentlemen.jpg",
      "youtube": "https://www.youtube.com/embed/2B0RpUGss2c",
      "recommended": true
    }
    , null, 2
    );
  }

  public exampleInputForCreateScreening(): void {
  this.exampleCreateScreening = JSON.stringify(
      {
        "movie": "5f63fe4b45c90e2cd57e04d4",
        "date": "2020-12-13T13:00:00Z",
        "showtime": "13:00h-15:08h",
        "cinemaHall": "5f63ff4245c90e2cd57e04d6",
        "seats": [
          [
            {
              "seatId": "A1",
              "booked": false
            },
            {
              "seatId": "A2",
              "booked": false
            },
            {
              "seatId": "A3",
              "booked": false
            },
            {
              "seatId": "A4",
              "booked": false
            },
            {
              "seatId": "A5",
              "booked": false
            }
          ],
          [
            {
              "seatId": "B1",
              "booked": false
            },
            {
              "seatId": "B2",
              "booked": false
            },
            {
              "seatId": "B3",
              "booked": false
            },
            {
              "seatId": "B4",
              "booked": false
            },
            {
              "seatId": "B5",
              "booked": false
            }
          ]
        ]
      }
      , null, 2
      );
  window.alert('Polja movie i cinemaHall su reference na id iz baza movies i cinemahalls');
  }

  public exampleInputForCreateCinemaHall(): void {
    this.exampleCreateCinemaHall = JSON.stringify({
      "name": "843",
      "seats": [
        [
          "A1",
          "A2",
          "A3",
          "A4",
          "A5"
        ],
        [
          "B1",
          "B2",
          "B3",
          "B4",
          "B5"
        ]
      ]
    }
    , null, 2
    );
  }

  public exampleInputForCreateInfo(): void {
    this.exampleCreateInfo = JSON.stringify({
      "name" : "CINEMATF",
      "email" : "cinematf@matf.bg.ac.rs",
      "address" : "Studentski trg 16, Beograd 105104",
      "telephone" : "011 2027801"
}
, null, 2
);
  }

}

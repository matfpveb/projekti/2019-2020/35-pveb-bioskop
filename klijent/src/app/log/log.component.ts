import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.css']
})
export class LogComponent implements OnInit {
  admin = false;

  constructor(private usersService: UsersService, private router: Router) {
    if (usersService.isAdmin()) {
      this.admin = true;
    }
   }

  ngOnInit(): void {
    if (!this.usersService.isLogged()){
      this.router.navigateByUrl('/login');
    }
  }

  public logout(): void{
    this.usersService.deleteToken();
    this.router.navigateByUrl('/login');
  }

  public getUsername(): string{
    return this.usersService.getUsername();
  }
}

import { map, switchMap } from 'rxjs/operators';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

import { Screening } from '../models/screenings.model';
import { User } from './../models/user.model';
import { ScreeningsService } from './../services/screenings.service';
import { UsersService } from './../services/users.service';
import { SeatReservationService } from './../services/seat-reservation.service';
import { Reservation } from './../models/reservation.model';
import { ReservationService } from './../services/reservation.service';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit, OnDestroy {
  public screening: Screening;
  public user: User;
  private activeSubscriptions: Subscription[];
  public seatsReservedByUser: Array<string>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private screeningService: ScreeningsService,
    private seatReservationService: SeatReservationService,
    private reservationService: ReservationService,
    private usersService: UsersService
  ) {
    this.activeSubscriptions = [];
    this.findScreeningById();
    this.findUserByUsername();

    // in case the user has selected seats to be reserved and then left the reservation without deselecting the seats
    // or if he has submitted the reservation
    // we reset the array of seats before allowing him to choose new ones
    this.seatReservationService.reset();
    this.seatsReservedByUser = this.seatReservationService.getSeatsReservedByUser();
  }

  ngOnInit(): void {
    if (!this.usersService.isLogged()) {
      this.router.navigateByUrl('/login');
    }
  }


  private findScreeningById() {
    const getScreeningSub = this.route.paramMap
      .pipe(
        map((params) => params.get('screeningId')),
        switchMap((screeningIdParam) =>
          this.screeningService.getScreeningById(screeningIdParam)
        )
      )
      .subscribe((screening) => (this.screening = screening));

    this.activeSubscriptions.push(getScreeningSub);
  }

  private findUserByUsername() {
    const username = this.usersService.getUsername();
    const userSub = this.usersService
    // if not working change username to something from database to check
      .getUserByUsername(username)
      .subscribe((user) => {
        this.user = user;
      });

    this.activeSubscriptions.push(userSub);
  }

  onSubmit(): void {
    // allow only if at least one seat was reserved
    if (this.seatsReservedByUser.length > 0) {

      // update booked seats for this screening
      const screenSub = this.screeningService
        .updateBookedSeats(this.screening.seats, this.screening._id)
        .subscribe();

      this.activeSubscriptions.push(screenSub);

      // make reservation
      const reservation = {user: this.user, screening: this.screening, seats: this.seatsReservedByUser};

      const resSub = this.reservationService
        .postReservation(reservation)
        .subscribe((res: Reservation) => {
          window.alert(`Rezervacija uspešna! Identifikacija rezervacije: ${ res._id }. Uzivajte u filmu. :) `);
          this.router.navigate(['/']);
        });

      this.activeSubscriptions.push(resSub);
    } else {
      window.alert('Morate rezervisati barem jedno sedište. :)');
    }
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }

}

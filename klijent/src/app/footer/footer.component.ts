import { Component, OnInit, OnDestroy } from '@angular/core';
import {InfoService } from '../services/info.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit, OnDestroy {

  telephone: string;
  name: string;
  address: string;
  email: string;

  activeSubscriptions: Subscription[] = [];

  constructor(private infoSer: InfoService) {
      const sub = infoSer.refreshInfo().subscribe( infos => {
        const info = infos[0];
        this.telephone = info.telephone;
        this.address = info.address;
        this.email = info.email;
        this.name = info.name;
      });
      this.activeSubscriptions.push(sub);
   }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => {
      sub.unsubscribe(); });
  }

}

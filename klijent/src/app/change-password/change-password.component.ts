import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../services/users.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  public logForm: FormGroup;
  activeSubscriptions: Subscription[] = [];

  constructor(private usersService: UsersService, private formBuilder: FormBuilder) {
    this.logForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      curPassword: ['', [Validators.required]],
      newPassword: ['', [Validators.required, Validators.pattern('.{4,}')]]
    });
   }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => {
      sub.unsubscribe(); });
  }

  public submitForm(data): void{
    if (!this.logForm.valid){
      window.alert('Nisu unete dobre vrednosti');
    }
    else{
      const sub = this.usersService.changePassword(data)
              .subscribe(res => {
        window.alert('Uspesna promena lozinke');
        this.logForm.reset();
       }, err => {
        window.alert('Neuspesna promena lozinke');
      });
      this.activeSubscriptions.push(sub);
    }
  }

  public get username() {
    return this.logForm.get('username');
  }
  public get curPassword() {
    return this.logForm.get('curPassword');
  }
  public get newPassword() {
    return this.logForm.get('newPassword');
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../services/users.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  public logForm: FormGroup;
  activeSubscriptions: Subscription[] = [];

  constructor(private usersService: UsersService, private router: Router, private formBuilder: FormBuilder) {
    this.logForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
   }

  ngOnInit(): void {
    if (this.usersService.isLogged()){
      this.router.navigateByUrl('/logout');
    }
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => {
      sub.unsubscribe(); });
  }

  public isLoged(): boolean {
    return this.usersService.isLogged();
  }

  public submitForm(data): void{
    if (!this.logForm.valid){
      window.alert('Nisu unete dobre vrednosti');
    }
    else{
      const sub = this.usersService.authenticateUser(data).subscribe(res => {
        this.usersService.setToken(res.token);
        this.router.navigateByUrl('/logout');
      },
      err => {
        window.alert('Neuspesno logovanje');
      });
      this.activeSubscriptions.push(sub);
    }
  }

  public get username() {
    return this.logForm.get('username');
  }
  public get password() {
    return this.logForm.get('password');
  }
}

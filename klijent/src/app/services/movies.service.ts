import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Movie } from './../models/movie.model';
import { HttpErrorHandler } from '../models/http-error-handler.model';

@Injectable({
  providedIn: 'root'
})
export class MoviesService extends HttpErrorHandler {
  private movies: Observable<Movie[]>;
  private recommendedMovies: Observable<Movie[]>;
  private readonly moviesUrl = 'http://localhost:3000/movies/';

  constructor(private http: HttpClient, router: Router) {
    super(router);
    this.refreshMovies();
   }

   private refreshMovies(): Observable<Movie[]> {
    this.movies = this.http
      .get<Movie[]>(this.moviesUrl)
      .pipe(
        catchError(super.handleError())
      );
    return this.movies;
   }

   public getMovies(): Observable<Movie[]> {
    return this.movies;
   }

   public getMovieById(id: string): Observable<Movie> {
    return this.http
      .get<Movie>(this.moviesUrl + id)
      .pipe(
        catchError(super.handleError())
      );
   }

   public getRecommendedMovies(): Observable<Movie[]> {
    this.recommendedMovies = this.http
      .get<Movie[]>(this.moviesUrl + 'recommended')
      .pipe(
        catchError(super.handleError())
      );
    return this.recommendedMovies;
   }

   public  createMovie(data) {
     return this.http.post(this.moviesUrl, data, {headers: new HttpHeaders({"token": window.localStorage.getItem('token')})})
     .pipe(
      catchError(super.handleError())
    );
   }

   public deleteMovie(id) {
    return this.http.delete(this.moviesUrl + id, {headers: new HttpHeaders({"token": window.localStorage.getItem('token')})})
    .pipe(
      catchError(super.handleError())
    );
  }
}

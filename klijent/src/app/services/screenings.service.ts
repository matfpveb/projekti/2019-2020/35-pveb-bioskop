import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Screening } from './../models/screenings.model';
import { HttpErrorHandler } from '../models/http-error-handler.model';

@Injectable({
  providedIn: 'root'
})
export class ScreeningsService extends HttpErrorHandler {
  private screenings: Observable<Screening[]>;
  private readonly screeningsUrl = 'http://localhost:3000/screenings/';

  constructor(private http: HttpClient, router: Router) {
    super(router);
    this.refreshScreenings();
   }

  private refreshScreenings(): Observable<Screening[]> {
    this.screenings = this.http
      .get<Screening[]>(this.screeningsUrl)
      .pipe(
        catchError(super.handleError())
      );
    return this.screenings;
  }

  public getScreenings(): Observable<Screening[]> {
    return this.screenings;
  }

  public getScreeningById(id: string): Observable<Screening> {
    return this.http
      .get<Screening>(this.screeningsUrl + id)
      .pipe(
        catchError(super.handleError())
      );
  }

  // only upcoming screenings for a specific movie
  public getUpcomingScreeningsByMovieId(movieId: string): Observable<Screening[]> {
    return this.http
      .get<Screening[]>(this.screeningsUrl + 'movie/' + movieId)
      .pipe(
        catchError(super.handleError())
      );
  }

  public updateBookedSeats(seats, id: string): Observable<Screening>{
    return this.http
      .patch<Screening>(this.screeningsUrl + id, {seats}, {headers: new HttpHeaders({"token": window.localStorage.getItem('token')})})
      .pipe(
        catchError(super.handleError())
      );
  }

  public createScreening(data) {
    return this.http.post(this.screeningsUrl, data, {headers: new HttpHeaders({"token": window.localStorage.getItem('token')})})
    .pipe(
      catchError(super.handleError())
    );
  }

  public deleteScreening(id) {
    return this.http.delete(this.screeningsUrl + id, {headers: new HttpHeaders({"token": window.localStorage.getItem('token')})})
    .pipe(
      catchError(super.handleError())
    );
  }

}

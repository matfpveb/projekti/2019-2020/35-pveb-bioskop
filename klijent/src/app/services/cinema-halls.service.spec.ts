import { TestBed } from '@angular/core/testing';

import { CinemaHallsService } from './cinema-halls.service';

describe('CinemaHallsService', () => {
  let service: CinemaHallsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CinemaHallsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, pipe } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Reservation } from './../models/reservation.model';
import { HttpErrorHandler } from '../models/http-error-handler.model';

@Injectable({
  providedIn: 'root'
})
export class ReservationService extends HttpErrorHandler {
  private reservations: Observable<Reservation[]>;
  private readonly reservationsUrl = 'http://localhost:3000/reservations/';

  constructor(private http: HttpClient, router: Router) {
    super(router);
  }

  // only get reservations for upcoming screeenings
  public getReservationsByUser(username: string): Observable<Reservation[]> {
      return this.http.get<Reservation[]>(this.reservationsUrl + 'user/' + username)
        .pipe(
          catchError(super.handleError())
        );
  }

  public getReservationById(id: string): Observable<Reservation> {
    return this.http.get<Reservation>(this.reservationsUrl + id)
      .pipe(
        catchError(super.handleError())
      );
}

  public postReservation(reservationInfo): Observable<Reservation> {
    return this.http
      .post<Reservation>(this.reservationsUrl, reservationInfo, {headers: new HttpHeaders({"token": window.localStorage.getItem('token')})})
      .pipe(
        catchError(super.handleError())
      );
  }

  public deleteReservationById(id: string): Observable<void> {
    return this.http
    .delete<void>(this.reservationsUrl + id, {headers: new HttpHeaders({"token": window.localStorage.getItem('token')})})
    .pipe(
      catchError(super.handleError())
    );
  }
}

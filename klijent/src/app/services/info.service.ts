import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpErrorHandler } from '../models/http-error-handler.model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class InfoService extends HttpErrorHandler{
  private info;
  private readonly infoUrl = 'http://localhost:3000/info/';

  constructor(private http: HttpClient, router: Router) {
    super(router);
    this.refreshInfo();
   }

   public refreshInfo(){
    this.info = this.http.get(this.infoUrl)
     .pipe(
      catchError(super.handleError())
    );
    return this.info;
   }

   public updateInfo(data){
    return this.info = this.http.post(this.infoUrl, data
      , {headers: new HttpHeaders({"token": window.localStorage.getItem('token')})})
       .pipe(
        catchError(super.handleError())
      );
   }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { User } from '../models/user.model';
import { Token } from '../models/token.model';
import { Message } from '../models/message.model';
import { HttpErrorHandler } from '../models/http-error-handler.model';

@Injectable({
  providedIn: 'root',
})
export class UsersService extends HttpErrorHandler {
  private readonly usersUrl = 'http://localhost:3000/users/';

  constructor(private http: HttpClient, router: Router) {
    super(router);
  }

  public registerUser(data) {
    return this.http
      .post(this.usersUrl + 'register', data)
      .pipe(catchError(super.handleError()));
  }

  public authenticateUser(data) {
    return this.http
      .post<Token>(this.usersUrl + 'authenticate', data)
      .pipe(catchError(super.handleError()));
  }

  public changePassword(data) {
    return this.http
      .post(this.usersUrl + 'changepassword', data)
      .pipe(catchError(super.handleError()));
  }

  public isAdmin(): boolean{
    const userPayload = this.getUserPayload();
    if (userPayload == null) {
      return false;
    }
    if (userPayload.rights === 'admin'
    && userPayload.exp > Date.now() / 1000) {
      return true;
    }
    else {
      return false;
    }
  }

  public isLogged(): boolean{
    const userPayload = this.getUserPayload();
    if (userPayload == null) {
      return false;
    }
    if ((userPayload.rights === 'admin' || userPayload.rights === 'user')
    && userPayload.exp > Date.now() / 1000) {
      return true;
    }
    else {
      return false;
    }
  }

  public setToken(token: string): void {
    window.localStorage.setItem('token', token);
  }

  public getToken(): string {
    return window.localStorage.getItem('token');
  }

  public deleteToken(): void {
    window.localStorage.removeItem('token');
  }

  public getUserPayload() {
    const token = this.getToken();
    if (token) {
      const userPayload = atob(token.split('.')[1]);
      return JSON.parse(userPayload);
    }
    else{
      return null;
    }
  }

  public getUsername(): string{
    const userPayload = this.getUserPayload();
    if (userPayload == null) {
      return '';
    }
    else {
      return userPayload.username;
    }
  }

  public getUserByUsername(username: string): Observable<User>{
    return this.http
      .get<User>(this.usersUrl + username,
        {headers: new HttpHeaders({"token": window.localStorage.getItem('token')})});
  }

}

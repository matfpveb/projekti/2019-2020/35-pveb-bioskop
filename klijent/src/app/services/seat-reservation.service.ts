import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SeatReservationService {
  private seatsReservedByUser: Array<string> = [];

  constructor() { }

  public getSeatsReservedByUser(): Array<string> {
    return this.seatsReservedByUser;
  }

  public addSeat(seat: string): void {
    this.seatsReservedByUser.push(seat);
  }

  public removeSeat(seat: string): void {
    const index: number = this.seatsReservedByUser.indexOf(seat);
    this.seatsReservedByUser.splice(index, 1);
  }

  public reset(): void {
    this.seatsReservedByUser = [];
  }

}

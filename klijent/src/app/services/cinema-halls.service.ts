import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { CinemaHall } from '../models/cinemaHall.model';
import { HttpErrorHandler } from '../models/http-error-handler.model';

@Injectable({
  providedIn: 'root'
})
export class CinemaHallsService extends HttpErrorHandler {
  private cinemaHalls: Observable<CinemaHall[]>;
  private readonly cinemaHallsUrl = 'http://localhost:3000/cinemahalls/';

  constructor(private http: HttpClient, router: Router) {
    super(router);
    this.refreshCinemaHalls();
   }

   private refreshCinemaHalls(): Observable<CinemaHall[]> {
    this.cinemaHalls = this.http
      .get<CinemaHall[]>(this.cinemaHallsUrl)
      .pipe(
        catchError(super.handleError())
      );
    return this.cinemaHalls;
   }

   public getCinemaHalls(): Observable<CinemaHall[]> {
    return this.cinemaHalls;
   }

   public  createCinemaHall(data) {
     return this.http.post(this.cinemaHallsUrl, data, {headers: new HttpHeaders({"token": window.localStorage.getItem('token')})})
     .pipe(
      catchError(super.handleError())
    );
   }

   public deleteCinemaHall(id) {
    return this.http.delete(this.cinemaHallsUrl + id, {headers: new HttpHeaders({"token": window.localStorage.getItem('token')})})
    .pipe(
      catchError(super.handleError())
    );
  }
}

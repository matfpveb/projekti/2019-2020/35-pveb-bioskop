import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViseInfoComponent } from './vise-info.component';

describe('ViseInfoComponent', () => {
  let component: ViseInfoComponent;
  let fixture: ComponentFixture<ViseInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViseInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViseInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

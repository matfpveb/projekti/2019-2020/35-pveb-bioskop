import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';

import { Movie } from './../models/movie.model';
import { MoviesService } from './../services/movies.service';
import { ScreeningsService } from './../services/screenings.service';
import { Screening } from '../models/screenings.model';


@Component({
  selector: 'app-vise-info',
  templateUrl: './vise-info.component.html',
  styleUrls: ['./vise-info.component.css']
})
export class ViseInfoComponent implements OnDestroy {
  public movie: Movie;
  public screenings: Screening[];
  private activeSubscriptions: Subscription[];

  constructor(
    private route: ActivatedRoute,
    private movieService: MoviesService,
    private screeningService: ScreeningsService
  ) {
    this.activeSubscriptions = [];
    this.findMovieById();
    this.findUpcomingScreeningsByMovieId();
  }

  private findMovieById() {
    const getMovieSub = this.route.paramMap
      .pipe(
        map((params) => params.get('filmId')),
        switchMap((movieIdParam) =>
          this.movieService.getMovieById(movieIdParam)
        )
      )
      .subscribe((movie) => (this.movie = movie));

    this.activeSubscriptions.push(getMovieSub);
  }

  private findUpcomingScreeningsByMovieId() {
    const getScreeningSub = this.route.paramMap
      .pipe(
        map((params) => params.get('filmId')),
        switchMap((movieIdParam) =>
          this.screeningService.getUpcomingScreeningsByMovieId(movieIdParam)
        )
      )
      .subscribe((screenings: Screening[]) => {
        this.screenings = screenings;
      });

    this.activeSubscriptions.push(getScreeningSub);
  }


  ngOnDestroy() {
    this.activeSubscriptions.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }

}

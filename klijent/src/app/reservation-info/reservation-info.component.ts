import { UsersService } from './../services/users.service';
import { Subscription, Observable } from 'rxjs';
import { Component, Input, OnDestroy } from '@angular/core';
import { Reservation } from '../models/reservation.model';
import { Screening } from '../models/screenings.model';
import { ReservationService } from '../services/reservation.service';
import { ScreeningsService } from './../services/screenings.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reservation-info',
  templateUrl: './reservation-info.component.html',
  styleUrls: ['./reservation-info.component.css']
})
export class ReservationInfoComponent implements OnDestroy {
  @Input()
  reservation: Reservation;
  screening: Screening;
  private activeSubscriptions: Subscription[];

  constructor(
    private reservationService: ReservationService,
    private screeningService: ScreeningsService,
    private usersService: UsersService,
    private router: Router
    ) {
      this.activeSubscriptions = [];
    }

  // if the user wishes to call off a reservation, we need to update the reserved seats for the appropriate screening
  // and delete the reservation
  deleteRes(): void {
    const seats = this.reservation.seats;
    this.screening = (this.reservation.screening as Screening);
    this.updateSeatsForScreening(seats);

    const delSub = this.reservationService.deleteReservationById(this.reservation._id).subscribe(res => {
      window.alert('Rezervacija je uspešno uklonjena. :)');
      this.router.navigate(['/']);
    }, err => {
      window.alert('Greska!)');
    });
    this.activeSubscriptions.push(delSub);
  }

  private updateSeatsForScreening(seats: Array<string>): void {
    seats.forEach(seat => {
      this.screening.seats.forEach(row => {
        row.forEach(seatInRow => {
          if (seatInRow.seatId === seat) {
            seatInRow.booked = false;
          }
        });
      });
    });

    const screenSub = this.screeningService
      .updateBookedSeats(this.screening.seats, this.screening._id)
      .subscribe();

    this.activeSubscriptions.push(screenSub);
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }

}

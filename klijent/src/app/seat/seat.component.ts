import { Component, Input, OnInit } from '@angular/core';
import { SeatReservationService } from './../services/seat-reservation.service';

@Component({
  selector: 'app-seat',
  templateUrl: './seat.component.html',
  styleUrls: ['./seat.component.css']
})
export class SeatComponent implements OnInit {
  @Input()
  public seat: {seatId: string, booked: boolean};
  public seatsReservedByUser: Array<string>; // contains only seats that are currently being reserved by a user

  color = 'white';

  constructor(private seatReservationService: SeatReservationService) {
    this.seatsReservedByUser = this.seatReservationService.getSeatsReservedByUser();
  }

  // keeping track of fill change with ngStyle
  setFill() {
    // if the seat was previously booked by another user set fill color to red
    const index: number = this.seatsReservedByUser.indexOf(this.seat.seatId);
    if (this.seat.booked === true && index === -1) {
      this.color = 'red';
    }

    return {
      fill: this.color
    }
  }

  reserveSeat() {
    // the seat is white...allow it to be booked
    if (this.color === 'white') {
      this.bookSeat();
    } else if (this.color === 'rebeccapurple') {
      // the seat is rebeccapurple which means
      // the seat is in the array "seatsReservedByUser"
      // the user has clicked twice on the same seat
      // he wants to withdrow the booking of that seat

      this.unbookSeat();
    } else { // the seat is red - it was previously reserved by another user
      window.alert('Ova stolica je već rezervisana :/. Molimo odaberite drugu. :)');
    }
  }

  private bookSeat(): void {
    // set fill color to rebeccapurple
    this.color = 'rebeccapurple';

    // put it into the reserved array
    this.seatReservationService.addSeat(this.seat.seatId);

    // set booked to true
    this.seat.booked = true;
  }

  private unbookSeat(): void {
    // set fill color to white
    this.color = 'white';

    // remove form array
    this.seatReservationService.removeSeat(this.seat.seatId);

    // set booked to false
    this.seat.booked = false;
  }

  ngOnInit(): void {
  }

}

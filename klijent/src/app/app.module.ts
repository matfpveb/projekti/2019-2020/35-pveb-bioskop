import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatCarouselModule } from '@ngmodule/material-carousel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigacijaComponent } from './navigacija/navigacija.component';
import { KorpaComponent } from './korpa/korpa.component';
import { FilmoviComponent } from './filmovi/filmovi.component';
import { FilmoviPreporucujemoComponent } from './filmovi-preporucujemo/filmovi-preporucujemo.component';
import { CarouselComponent } from './carousel/carousel.component';
import { FooterComponent } from './footer/footer.component';
import { LogComponent } from './log/log.component';
import { ViseInfoComponent } from './vise-info/vise-info.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SafePipe } from './pipes/safe.pipe';
import { AdminComponent } from './admin/admin.component';
import { ReservationComponent } from './reservation/reservation.component';
import { CinemaHallComponent } from './cinema-hall/cinema-hall.component';
import { SeatComponent } from './seat/seat.component';
import { MovieComponent } from './movie/movie.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { ReservationInfoComponent } from './reservation-info/reservation-info.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigacijaComponent,
    KorpaComponent,
    FilmoviComponent,
    FilmoviPreporucujemoComponent,
    CarouselComponent,
    FooterComponent,
    LogComponent,
    ViseInfoComponent,
    RegisterComponent,
    LoginComponent,
    ChangePasswordComponent,
    SafePipe,
    AdminComponent,
    ReservationComponent,
    CinemaHallComponent,
    SeatComponent,
    MovieComponent,
    ErrorPageComponent,
    ReservationInfoComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCarouselModule.forRoot(),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

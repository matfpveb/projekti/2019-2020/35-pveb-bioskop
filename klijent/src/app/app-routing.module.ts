import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FilmoviPreporucujemoComponent} from './filmovi-preporucujemo/filmovi-preporucujemo.component';
import {FilmoviComponent} from './filmovi/filmovi.component';
import { ViseInfoComponent } from './vise-info/vise-info.component';
import { ReservationComponent } from './reservation/reservation.component';
import {KorpaComponent} from './korpa/korpa.component';
import { LogComponent } from './log/log.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AdminComponent } from './admin/admin.component';
import { ErrorPageComponent } from './error-page/error-page.component';

const routes: Routes = [
  {path: '', component: FilmoviPreporucujemoComponent},
  {path: 'filmovi', component: FilmoviComponent},
  {path: 'filmovi/:filmId', component: ViseInfoComponent},
  {path: 'korpa', component: KorpaComponent},
  {path: 'reservation/:screeningId', component: ReservationComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'changepassword', component: ChangePasswordComponent},
  {path: 'logout', component: LogComponent},
  {path: 'admin', component: AdminComponent},
  {path: 'error', component: ErrorPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

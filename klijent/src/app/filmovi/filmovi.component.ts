import { Movie } from './../models/movie.model';
import { Observable } from 'rxjs';
import { MoviesService } from './../services/movies.service';
import { Component, OnInit } from '@angular/core';

import { FormControl } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-filmovi',
  templateUrl: './filmovi.component.html',
  styleUrls: ['./filmovi.component.css']
})
export class FilmoviComponent implements OnInit {
  movies$: Observable<Movie[]>;
  filteredMovies$: Observable<Movie[]>;
  filter: FormControl;
  filter2: FormControl;
  filter$: Observable<string>;
  filter2$: Observable<string>;

  constructor(private moviesService: MoviesService) {
    this.movies$ = this.moviesService.getMovies();
    this.filter = new FormControl('');
    this.filter2 = new FormControl('');
    this.filter$ = this.filter.valueChanges.pipe(startWith(''));
    this.filter2$ = this.filter2.valueChanges.pipe(startWith(''));
    this.filteredMovies$ = combineLatest([this.movies$, this.filter$, this.filter2$]).pipe(
      map(([movies, filterString, filterString2]) => movies.filter(
          movie => (movie.name.toLowerCase().indexOf(filterString.toLowerCase()) !== -1 &&
          movie.genre.toLowerCase().indexOf(filterString2.toLowerCase()) !== -1)
        )
      )
    );
  }

  ngOnInit(): void {
  }

}

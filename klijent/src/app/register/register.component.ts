import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsersService } from '../services/users.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
  public logForm: FormGroup;
  activeSubscriptions: Subscription[] = [];

  constructor(private usersService: UsersService, private formBuilder: FormBuilder) {
    this.logForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.pattern('[a-zA-Z]+[a-zA-Z0-9_]*')]],
      password: ['', [Validators.required, Validators.pattern('.{4,}')]]
    });
   }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => {
      sub.unsubscribe(); });
  }

  public submitForm(data): void{
    if (!this.logForm.valid){
      window.alert('Nisu unete dobre vrednosti');
    }
    else{
      const sub = this.usersService.registerUser(data)
      .subscribe(res => {
        window.alert('Uspesno prijavljivanje');
        this.logForm.reset();
       }, err => {
        window.alert('Neuspesno prijavljivanje! Korisnicko ime vec postoji.');
      });
      this.activeSubscriptions.push(sub);
    }
  }

  public get username() {
    return this.logForm.get('username');
  }
  public get password() {
    return this.logForm.get('password');
  }
}

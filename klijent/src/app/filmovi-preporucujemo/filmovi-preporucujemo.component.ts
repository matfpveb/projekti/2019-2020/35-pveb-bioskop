import { Movie } from './../models/movie.model';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { MoviesService } from './../services/movies.service';
@Component({
  selector: 'app-filmovi-preporucujemo',
  templateUrl: './filmovi-preporucujemo.component.html',
  styleUrls: ['./filmovi-preporucujemo.component.css']
})
export class FilmoviPreporucujemoComponent implements OnInit {
  public recommendedMovies: Observable<Movie[]>;

  constructor(private moviesService: MoviesService) {
    this.recommendedMovies = this.moviesService.getRecommendedMovies();
  }

  ngOnInit(): void {
  }

}

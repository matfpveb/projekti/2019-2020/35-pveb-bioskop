import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilmoviPreporucujemoComponent } from './filmovi-preporucujemo.component';

describe('FilmoviPreporucujemoComponent', () => {
  let component: FilmoviPreporucujemoComponent;
  let fixture: ComponentFixture<FilmoviPreporucujemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilmoviPreporucujemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilmoviPreporucujemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

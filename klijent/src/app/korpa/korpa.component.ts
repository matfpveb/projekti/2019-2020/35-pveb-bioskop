import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { UsersService } from '../services/users.service';
import { ReservationService } from './../services/reservation.service';
import { ScreeningsService } from './../services/screenings.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Reservation } from '../models/reservation.model';
import { Screening } from '../models/screenings.model';
import { User } from '../models/user.model';

@Component({
  selector: 'app-korpa',
  templateUrl: './korpa.component.html',
  styleUrls: ['./korpa.component.css']
})
export class KorpaComponent implements OnInit, OnDestroy {
  private activeSubscriptions: Subscription[];
  reservations: Observable<Reservation[]>;
  screening: Screening;
  reservationToBeDeleted;
  public username: string;

  constructor(
    private usersService: UsersService,
    private reservationService: ReservationService,
    private screeningService: ScreeningsService,
    private route: ActivatedRoute,
    private router: Router
    ) {
      this.username = this.usersService.getUsername();
      this.reservations = this.reservationService.getReservationsByUser(this.username);
      this.activeSubscriptions = [];
  }

  ngOnInit(): void {
    console.log(this.usersService.isLogged());
    if (!this.usersService.isLogged()) {
      this.router.navigateByUrl('/login');
    }
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }

}

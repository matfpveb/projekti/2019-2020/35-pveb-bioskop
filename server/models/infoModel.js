const mongoose = require('mongoose');

const infoSchema = new  mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true
    },
    address: {
      type: String,
      required: true
    },
    telephone: {
      type: String,
      required: true
    }
  });
  
  const infoModel = mongoose.model('Info', infoSchema);
  
  module.exports = infoModel;
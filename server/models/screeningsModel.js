const mongoose = require('mongoose');

const screeningsSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    movie: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Movie',
        required: true  
    },
    date: {       // contains date of screening and start time
        type: Date,
        required: true  
    },
    showtime: {   // example value: "17:30-18:30"
        type: String,
        required: true
    },
    cinemaHall: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Cinemahall',
        required: true
    },
    seats: {
        type: [[{seatId: String, booked: Boolean}]],
        required: true
    }
});

const screeningsModel = mongoose.model('Screening', screeningsSchema);

module.exports = screeningsModel;

const mongoose = require('mongoose');

const reservationsSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    screening: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Screening',
        required: true
    },
    seats: {
        type: [String],
        required: true
    }
});

const reservationsModel = mongoose.model('Reservation', reservationsSchema);

module.exports = reservationsModel;
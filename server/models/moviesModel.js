const mongoose = require('mongoose');

const moviesSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true  
    },
    synopsis: {
        type: String,
        required: true  
    },
    genre: {
        type: String,
        required: true
    },
    director: {
        type: String,
        required: true
    },
    actors: {
        type: String,
        required: true
    },
    duration: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    // example: https://www.youtube.com/embed/DBfsgcswlYQ
    // so the input should be https://www.youtube.com/embed/videoID
    youtube: {
        type: String,
        required: true
    },
    recommended: {
        type: Boolean,
        required: true
    },
    // should only be added if we rocommend the movie
    carouselImage: String
});

const moviesModel = mongoose.model('Movie', moviesSchema);

module.exports = moviesModel;
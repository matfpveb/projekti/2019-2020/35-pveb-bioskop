const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const usersSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true  
    },
    rights: {
        type: String,
        default: "user"
    },
    salt: String
});

usersSchema.pre('save', function(next){
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(this.password, salt, (err, hash) => {
            this.password = hash;
            this.salt = salt;
            next();
        });
    });
});

usersSchema.methods.verifyPassword = function(password){
    return bcrypt.compareSync(password, this.password);
};

usersSchema.methods.generateJwt = function() {
    return jwt.sign({username: this.username, rights : this.rights},
    'secretkey12345', {expiresIn: "12h"});
}

usersSchema.methods.verifyAdmin = function(token){
    if(token){
        jwt.verify(token, 'secretkey12345', (err, decoded) => {
            if(!err){
                return decoded.rights === 'admin';
            }
        });
    }
    return false;
}

const usersModel = mongoose.model('User', usersSchema);

module.exports = usersModel;

const mongoose = require('mongoose');

const cinemaHallsSchema = new  mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
      type: String,
      required: true
    },
    seats: {
      type: [[String]],
      required: true
    }
  });
  
  const cinemaHallsModel = mongoose.model('Cinemahall', cinemaHallsSchema);
  
  module.exports = cinemaHallsModel;
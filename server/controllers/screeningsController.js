const mongoose = require('mongoose');
const Screening = require('../models/screeningsModel');

module.exports.getScreenings = async (req, res, next) => {
  try {
    const screenings = await Screening.find({}).exec();
    res.status(200).json(screenings);
  } catch (err) {
    next(err);
  }
};

module.exports.getScreeningById = async (req, res, next) => {
  try {
    const screening = await Screening.findById(req.params.id).populate('movie').populate('cinemaHall').exec();
    if (screening) {
      res.status(200);
      res.json(screening);
    } else {
      res.status(404);
      res.send();
    }
  } catch (err) {
    next(err);
  }
};

// to convert UTC to local time and date
function convertUTCDateToLocalDate(date) {
  var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

  var offset = date.getTimezoneOffset() / 60;
  var hours = date.getHours();

  newDate.setHours(hours - offset);

  var i;
  if (offset < 0) {
    i = -offset;
  } else {
    i = offset;
  }

  while (i > -1) {
    if (hours === i) {
      newDate.setDate(newDate.getDate() + 1);
    }
    i = i-1;
  }

  return newDate;
}

// for a specific movie we will want to show only upcoming screenings
module.exports.getUpcomingScreeningsByMovieId = async (req, res, next) => {
  currentDate = convertUTCDateToLocalDate(new Date());
  try {
    const screenings = await Screening.find({movie: req.params.movieId, date: {$gt: currentDate}}).populate('movie', 'name').populate('cinemaHall', 'name').exec();
    res.status(200).json(screenings);
  } catch (err) {
    next(err);
  }
};

module.exports.createScreening = async (req, res, next) => {
  if (!req.body.movie || !req.body.date || !req.body.showtime || !req.body.cinemaHall || !req.body.seats) {
    res.status(400);
    res.send();
  } else {
    try {
      const newScreening = new Screening({
        _id: new mongoose.Types.ObjectId(),
        movie: req.body.movie,
        date: req.body.date,
        showtime: req.body.showtime,
        cinemaHall: req.body.cinemaHall,
        seats: req.body.seats,

      });

      await newScreening.save();

      res.status(201).json(newScreening);
    } catch (err) {
      next(err);
    }
  }
};

module.exports.updateReservedSeats = async (req, res, next) => {
  try {
    await Screening.updateOne(
      { _id: req.params.id },
      { $set: { seats: req.body.seats } }
    ).exec();
    
    res.status(200).send();
  } catch (err) {
    next(err);
  }
};

module.exports.deleteScreeningById = async (req, res, next) => {
  try {
    const screening = await Screening.findById(req.params.id).exec();

    if (screening) {
      await Screening.findByIdAndDelete(req.params.id).exec();
      res.status(200).send();
    } else {
      res.status(404).send();
    }
  } catch (err) {
    next(err);
  }
};

const mongoose = require('mongoose');
const Reservation = require('../models/reservationsModel');

module.exports.getReservations = async (req, res, next) => {
  try {
    const reservations = await Reservation.find({}).exec();
    res.status(200).json(reservations);
  } catch (err) {
    next(err);
  }
};

module.exports.getReservationById = async (req, res, next) => {
  try {
    const reservation = await Reservation.findById(req.params.id).exec();
    if (reservation) {
      res.status(200);
      res.json(reservation);
    } else {
      res.status(404);
      res.send();
    }
  } catch (err) {
    next(err);
  }
};

// to convert UTC to local time and date
function convertUTCDateToLocalDate(date) {
  var newDate = new Date(date.getTime()+date.getTimezoneOffset()*60*1000);

  var offset = date.getTimezoneOffset() / 60;
  var hours = date.getHours();

  newDate.setHours(hours - offset);  
  
  var i;
  if (offset < 0) {
    i = -offset;
  } else {
    i = offset;
  }

  while (i > -1) {
    if (hours === i) {
      newDate.setDate(newDate.getDate() + 1);
    }
    i = i-1;
  }

  return newDate;   
}

const User = require('../models/usersModel');
const Screening = require('../models/screeningsModel');

module.exports.getReservationsByUser = async (req, res, next) => {
  const username = req.params.user;
  currentDate = convertUTCDateToLocalDate(new Date());
  try {
    
    const user = await User.find({username}).exec();
    const upcomingScreenings = await Screening.find({date: {$gt: currentDate}});
    const upcomingScreeningsIds = upcomingScreenings.map(scr => scr._id);
    
    const reservation = await Reservation.find({user: user, screening: {$in: upcomingScreeningsIds}})
                                          .populate({
                                            path: 'screening',
                                            // we want to populate movie in screening
                                            populate: { path: 'movie' }
                                          })
                                          .populate({
                                            path: 'screening',
                                            // we also want to populate cinemaHall in screening
                                            populate: { path: 'cinemaHall' }
                                          })
                                          .exec();

    res.status(200).json(reservation);
  } catch (err) {
    next(err);
  }
};

module.exports.createReservation = async (req, res, next) => {
  if (!req.body.user || !req.body.screening || !req.body.seats) {
    res.status(400);
    res.send();
  } else {
    try {
      const newReservation = new Reservation({
        _id: new mongoose.Types.ObjectId(),
        user: req.body.user,
        screening: req.body.screening,
        seats: req.body.seats,
      });

      await newReservation.save();

      res.status(201).json(newReservation);
    } catch (err) {
      next(err);
    }
  }
};

module.exports.deleteReservationById = async (req, res, next) => {
  try {
    const reservation = await Reservation.findById(req.params.id).exec();

    if (reservation) {
      await Reservation.findByIdAndDelete(req.params.id).exec();
      res.status(200).send();
    } else {
      res.status(404).send();
    }
  } catch (err) {
    next(err);
  }
};

const mongoose = require('mongoose');
const CinemaHall = require('../models/cinemaHallsModel');

module.exports.getCinemaHalls = async (req, res, next) => {
  try {
    const cinemaHalls = await CinemaHall.find({}).exec();
    res.status(200).json(cinemaHalls);
  } catch (err) {
    next(err);
  }
};

module.exports.getCinemaHallById = async (req, res, next) => {
  try {
    const cinemaHall = await CinemaHall.findById(req.params.id).exec();
    if (cinemaHall) {
      res.status(200);
      res.json(cinemaHall);
    } else {
      res.status(404);
      res.send();
    }
  } catch (err) {
    next(err);
  }
};

module.exports.createCinemaHall = async (req, res, next) => {
  if (!req.body.name || !req.body.seats) {
    res.status(400);
    res.send();
  } else {
    try {
      const newCinemaHall = new CinemaHall({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        seats: req.body.seats
      });

      await newCinemaHall.save();

      res.status(201).json(newCinemaHall);
    } catch (err) {
      next(err);
    }
  }
};

module.exports.deleteCinemaHallById = async (req, res, next) => {
  try {
    const cinemaHall = await CinemaHall.findById(req.params.id).exec();

    if (cinemaHall) {
      await CinemaHall.findByIdAndDelete(req.params.id).exec();
      res.status(200).send();
    } else {
      res.status(404).send();
    }
  } catch (err) {
    next(err);
  }
};

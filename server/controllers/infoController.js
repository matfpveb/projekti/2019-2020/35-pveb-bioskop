const mongoose = require('mongoose');
const Info = require('../models/infoModel');

module.exports.getInfo = async (req, res, next) => {
    try{
        const infos = await Info.find({}).exec();
        res.status(200).json(infos);
    }
    catch(err){
        next(err);
    }
}

  
  module.exports.updateInfo = async (req, res, next) => {
    try {
      const info = await Info.findOne().exec();
      if (info){
        if(req.body.name){
          info.name = req.body.name;
        }
        if(req.body.email){
          info.email = req.body.email;
        }
        if(req.body.address){
          info.address = req.body.address;
        }
        if(req.body.telephone){
          info.telephone = req.body.telephone;
        }
        await info.save();
        res.status(200).send();
      }
      else {
        if (!req.body.name || !req.body.email || !req.body.address || !req.body.telephone) {
          res.status(400);
          res.send();
        }
        else {
          const newInfo = new Info({
            _id: new mongoose.Types.ObjectId(),
            name: req.body.name,
            email: req.body.email,
            address: req.body.address,
            telephone: req.body.telephone,
          });

          await newInfo.save();

          res.status(201).json(newInfo);
        }
    }
    } catch (err) {
      next(err);
    }
}

  
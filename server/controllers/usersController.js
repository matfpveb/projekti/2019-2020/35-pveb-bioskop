const mongoose = require('mongoose');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../models/usersModel');

module.exports.register = async (req, res, next) => {
  if (!req.body.username || !req.body.password) {
    res.status(400).send();
  } else {
    try {
      const newUser = new User({
        _id: new mongoose.Types.ObjectId(),
        username: req.body.username,
        password: req.body.password
      });

      await newUser.save((err, doc) => {
        if(err){
          if(err.code == 11000){
            res.status(422).send();
          }
          else {
            return next(err);
          }
        }
        else{
          res.status(201).send();
        }
      });
    } catch (err) {
      next(err);
    }
  }
};

module.exports.authenticate = (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    if (err) return res.status(400).json({message: "Greska"});
    else if (user) return res.status(200).json({"token": user.generateJwt()});
    else return res.status(404).json({message: "Pogresna lozinka ili korisnicko ime"});  
  })(req, res);
}

module.exports.verify = (req, res, next) => {
  let token;
  if("token" in req.headers){
    token = req.headers["token"];
  }
  if(token){
      jwt.verify(token, 'secretkey12345', (err, decoded) => {
          if(!err){
              req.username = decoded.username;
              req.rights = decoded.rights;
              next();
          }
          else {
            res.status(404).json({message: "Bad token"});
          }
      });
  }
  else {
    res.status(403).json({message: "No token provided"});
  }
}

module.exports.changePassword = async (req, res, next) => {
  try {
    const updatePasswordInfo = req.body;
    const user = await User.findOne().where('username').equals(updatePasswordInfo.username).exec();

    if (user) {

      if (!user.verifyPassword(updatePasswordInfo.curPassword)) {
        res.status(400).send();
      } else {
        user.password = updatePasswordInfo.newPassword;
        await user.save();
        res.status(200).send();
      }
    } else {
      res.status(404).send();
    }
  } catch (err) {
    next(err);
  }
}

module.exports.getUserByUsername = async (req, res, next) => {
  try {
    const user = await User.findOne().where('username').equals(req.params.username).exec();
    if (user) {
      res.status(200);
      res.json(user);
    } else {
      res.status(404);
      res.send();
    }
  } catch (err) {
    next(err);
  }
};


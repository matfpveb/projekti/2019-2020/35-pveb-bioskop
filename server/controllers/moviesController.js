const mongoose = require('mongoose');
const Movie = require('../models/moviesModel');
const multer = require('multer');

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function(req, file, cb) {
    cb(null, file.originalname);
  }
});
const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  fileFilter: fileFilter
});

module.exports.getMovies = async (req, res, next) => {
  try {
    const movies = await Movie.find({}).exec();
    res.status(200).json(movies);
  } catch (err) {
    next(err);
  }
};

module.exports.getMovieById = async (req, res, next) => {
  try {
    const movie = await Movie.findById(req.params.id).exec();
    if (movie) {
      res.status(200);
      res.json(movie);
    } else {
      res.status(404);
      res.send();
    }
  } catch (err) {
    next(err);
  }
};

module.exports.getRecommendedMovies = async (req, res, next) => {
  try {
    const movies = await Movie.find({recommended: true}).exec();
    res.status(200).json(movies);
  } catch (err) {
    next(err);
  }
};

module.exports.createMovie = async (req, res, next) => {

  if (!req.body.name || !req.body.synopsis || !req.body.genre || !req.body.director || !req.body.actors || !req.body.duration || !req.body.youtube || req.body.recommended === undefined) {
    res.status(400);
    res.send();
  } else {
    try {
      const newMovie = new Movie({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        synopsis: req.body.synopsis,
        genre: req.body.genre,
        director: req.body.director,
        actors: req.body.actors,
        duration: req.body.duration,
        image: req.body.image,
        youtube: req.body.youtube,
        recommended: req.body.recommended
      });

      if (newMovie.recommended) {
        newMovie['carouselImage'] =  req.body.image;
      }

      await newMovie.save();

      res.status(201).json(newMovie);
    } catch (err) {
      next(err);
    }
  }
};

module.exports.deleteMovieById = async (req, res, next) => {
  try {
    const movie = await Movie.findById(req.params.id).exec();

    if (movie) {
      await Movie.findByIdAndDelete(req.params.id).exec();
      res.status(200).send();
    } else {
      res.status(404).send();
    }
  } catch (err) {
    next(err);
  }
};

const passport = require('passport');
const ls = require('passport-local').Strategy;
const mongoose = require('mongoose');

const User = require('./models/usersModel');

passport.use(new ls(
  function(username, password, done) {
    User.findOne({ username: username }, function (err, user) {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }
      if (!user.verifyPassword(password)) { return done(null, false); }
      return done(null, user);
      });
    }
));

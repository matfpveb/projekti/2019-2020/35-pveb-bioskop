require('./passport');

const express = require('express');
const mongoose = require('mongoose');
const { urlencoded, json } = require('body-parser');
const passport = require('passport');
const multer = require('multer');

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function(req, file, cb) {
      cb(null, file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  fileFilter: fileFilter
});


const app = express();

mongoose.connect('mongodb://localhost:27017/cinematf', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.use(json());
app.use(urlencoded({ extended: false }));
// added for images
app.use('/uploads', express.static('uploads'));

// Implementacija CORS zastite
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', ['Content-Type', 'token']);

  if (req.method === 'OPTIONS') {
    res.header(
      'Access-Control-Allow-Methods',
      'OPTIONS, GET, POST, PATCH, DELETE'
    );

    return res.status(200).json({});
  }

  next();
});

app.use(passport.initialize());

const infosContr = require('./controllers/infoController');
const usersContr = require('./controllers/usersController');

// http://localhost:3000/info
app.get('/info', infosContr.getInfo);

// http://localhost:3000/info
app.post('/info', usersContr.verify, infosContr.updateInfo);

// http://localhost:3000/users/:username
app.get('/users/:username', usersContr.verify, usersContr.getUserByUsername);

// http://localhost:3000/users/register
app.post('/users/register', usersContr.register);

// http://localhost:3000/users/authenticate
app.post('/users/authenticate', usersContr.authenticate);

// http://localhost:3000/users/1
app.post('/users/changepassword', usersContr.changePassword);

const cinemaHallsContr = require('./controllers/cinemaHallsController');

// http://localhost:3000/cinemaHalls
app.get('/cinemaHalls', cinemaHallsContr.getCinemaHalls);

// http://localhost:3000/cinemaHalls/1
app.get('/cinemaHalls/:id', cinemaHallsContr.getCinemaHallById);

// http://localhost:3000/cinemaHalls
app.post('/cinemaHalls', usersContr.verify, cinemaHallsContr.createCinemaHall);

// http://localhost:3000/cinemaHalls/1
app.delete('/cinemaHalls/:id', usersContr.verify, cinemaHallsContr.deleteCinemaHallById);

const moviesContr = require('./controllers/moviesController');

// http://localhost:3000/movies
app.get('/movies', moviesContr.getMovies);

// http://localhost:3000/movies/recommended
app.get('/movies/recommended', moviesContr.getRecommendedMovies);

// http://localhost:3000/movies/1
app.get('/movies/:id', moviesContr.getMovieById);

// http://localhost:3000/movies
app.post('/movies', usersContr.verify, upload.fields([{name: 'image'}, {name: 'carouselImage'}]), moviesContr.createMovie);

// http://localhost:3000/movies/1
app.delete('/movies/:id', usersContr.verify, moviesContr.deleteMovieById);

const reservationsContr = require('./controllers/reservationsController');

// http://localhost:3000/reservations
app.get('/reservations', reservationsContr.getReservations);

// http://localhost:3000/reservations/1
app.get('/reservations/:id', reservationsContr.getReservationById);

// http://localhost:3000/reservations
app.post('/reservations', usersContr.verify, reservationsContr.createReservation);

// http://localhost:3000/reservations/1
app.delete('/reservations/:id', usersContr.verify, reservationsContr.deleteReservationById);

// only get reservations for upcoming screeenings
// http://localhost:3000/reservations/user/1
app.get('/reservations/user/:user', reservationsContr.getReservationsByUser);

const screeningsContr = require('./controllers/screeningsController');

// http://localhost:3000/screenings
app.get('/screenings', screeningsContr.getScreenings);

// http://localhost:3000/screenings/1
app.get('/screenings/:id', screeningsContr.getScreeningById);

// http://localhost:3000/screenings/movie/1
app.get('/screenings/movie/:movieId', screeningsContr.getUpcomingScreeningsByMovieId);

// http://localhost:3000/screenings
app.post('/screenings', usersContr.verify, screeningsContr.createScreening);

// http://localhost:3000/screenings/1
app.patch('/screenings/:id', usersContr.verify, screeningsContr.updateReservedSeats);

// http://localhost:3000/screenings/1
app.delete('/screenings/:id', usersContr.verify, screeningsContr.deleteScreeningById);

app.use(function (error, req, res, next) {
  const statusCode = error.status || 500;
  res.status(statusCode).json({
    error: {
      message: error.message,
      status: statusCode,
      stack: error.stack
    },
  });
});

module.exports = app;